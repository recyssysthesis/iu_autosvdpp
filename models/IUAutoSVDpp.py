'''
Author: Abdelghani Azri
E-mail: abdelghani.azri@gmail.com
Inspired by: https://github.com/cheungdaven/autosvdpp
'''
import numpy as np
import time
import pickle
from math import ceil
import matplotlib.pyplot as plt

'''
Author: Abdelghani Azri
E-mail: abdelghani.azri@gmail.com
Inspired by: https://github.com/cheungdaven/autosvdpp
'''
import numpy as np
import time
import pickle
from math import ceil
import matplotlib.pyplot as plt

class IUAutoSVDpp():
    def __init__(self, item_features, user_features, epochs=10,num_factors=10,
                 gamma1=0.007, gamma2=0.007, lambda1=0.005, lambda2=0.015, phi=0.001, psi=0.001):
        self.epochs = epochs
        self.num_factors = num_factors
        self.gamma1 = gamma1
        self.gamma2 = gamma2
        self.lambda1 =lambda1
        self.lambda2 = lambda2
        self.phi = phi
        self.psi = psi
        self.item_features = item_features
        self.user_features = user_features

    def train(self, train_data, test_data):
        users, items = train_data.nonzero()
        uni_users = np.unique(users)

        num_of_ratings = len(users)
        sum_of_ratings = 0
        item_by_users = {}
        for u, i in zip(users,items):
            sum_of_ratings += int(str(ceil(train_data[u,i]))[:1])
            item_by_users.setdefault(u,[]).append(i)
        average_rating_train = float(sum_of_ratings/num_of_ratings)

        m, n = train_data.shape
        self.average_rating = average_rating_train
        self.item_by_users = item_by_users

        #randomly initial the parameters
        V = np.random.rand(self.num_factors, n) * 0.01
        U = np.random.rand(self.num_factors, m) * 0.01
        Y = np.random.rand(n,self.num_factors) * 0.0
        B_U = np.zeros(m)
        B_I = np.zeros(n)


        start_time = time.time()
        train_losses = [];
        for epoch in range(self.epochs):
            for u in uni_users:
                n_u = len(item_by_users[u])
                sqrt_n_u = np.sqrt(n_u)
                sum_y_j = np.zeros(self.num_factors)

                for j in item_by_users[u]:
                    sum_y_j += Y[j, :]
                    #print('y :', Y[j, :])
                p_im = sum_y_j / sqrt_n_u
                p_old = p_im
                #print('p :', p_im,p_old)

                for i in item_by_users[u]:
                    dot = np.dot((p_im + U[:,u] + self.psi * self.user_features[u]).T, (V[:,i]+ self.phi * self.item_features[i]))
                    error = int(str(ceil(train_data[u,i]))[:1]) - (average_rating_train + B_U[u] + B_I[i] + dot)

                    #update parameters
                    B_U[u] += self.gamma1 * (error - self.lambda1 * B_U[u])
                    B_I[i] += self.gamma1 * (error - self.lambda1 * B_I[i])
                    V[:, i] += self.gamma2 * (error * (p_im + U[:,u] + self.psi * self.user_features[u]) - self.lambda2 * V[:,i])
                    U[:, u] += self.gamma2 * (error * (V[:,i] + self.phi * self.item_features[i]) - self.lambda2 * U[:,u])
                    p_im += self.gamma2 * (error  * (V[:,i] + self.phi * self.item_features[i])- self.lambda2 * p_im)

                for item in item_by_users[u]:
                    Y[item, :] +=   (1/sqrt_n_u)  * ( p_im - p_old)

            self.B_U = B_U
            self.B_I = B_I
            self.V = V
            self.U = U
            self.Y = Y
            self.evaluate(test_data,train_losses)
        self.plotResult(train_losses)
        print("--- %s seconds --- in efficient caesvd++" % (time.time() - start_time))


    def evaluate(self, data_set, train_losses):
        users, items = data_set.nonzero()
        num_of_ratings = len(users)
        sum_for_rmse = 0
        sum_for_mae = 0
        for u, i in zip(users, items):
            n_u = len(self.item_by_users[u])
            sqrt_n_u = np.sqrt(n_u)
            sum_y_j = np.zeros(self.num_factors)
            for j in self.item_by_users[u]:
                sum_y_j += self.Y[j, :]
            dot = np.dot((sum_y_j / sqrt_n_u + self.U[:, u] + self.psi * self.user_features[u]).T, (self.V[:, i] + self.psi * self.item_features[i]))
            error = int(str(ceil(data_set[u,i]))[:1]) - (self.average_rating + self.B_U[u] + self.B_I[i] + dot)
            sum_for_rmse += error ** 2
            sum_for_mae += abs(error)
        rmse = np.sqrt(sum_for_rmse /num_of_ratings)
        mae = sum_for_mae/num_of_ratings
        train_losses.append(rmse)
        print("IUAutoSVD++ RMSE = {:.5f}".format(rmse), "IUAutoSVD++ MAE = {:.5f}".format(mae))
        return rmse,mae
    
    def plotResult(self, train_losses):
        plt.figure(figsize=(8,5))
        plt.plot(train_losses,label="IU-AutoSVD++", marker="", c="b",linestyle='solid')
        plt.xlabel("Epochs")
        plt.ylabel("RMSE (Movielens 100K)")
        plt.legend()
        plt.grid(linestyle = '--', linewidth = 0.5)
        plt.show()
