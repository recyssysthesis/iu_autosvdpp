import pandas as pd
import numpy as np
from scipy.sparse import csc_matrix
from sklearn.model_selection import train_test_split

class LoadData():

    PATH_100K = 'data/ml-100k/u.data'
    PATH_1M= 'data/ml-1m/movies_rating_new_id.csv'

    def __init__(self, test_ratio = 0.1):
        '''
        Intial the LoadData Class
        :param test_ratio: given test data ratio when loading data, default 0.1
        '''
        self.test_ratio = test_ratio

    def loadMovielens100K(self, path=PATH_100K):
        '''
        Load the Movielens 100k data set
        :param path: path of the dataset
        :return: train data and test data
        '''
        header = ['user_id', 'item_id', 'rating', 'timestamp']
        df = pd.read_csv(path, sep='\t', names=header)
        n_users = df.user_id.unique().shape[0]
        n_items = df.item_id.unique().shape[0]
        print("Number of users=" + str(n_users) + "; Number of items=" + str(n_items))
        train_data, test_data = train_test_split(df, test_size=self.test_ratio)
        train_data = pd.DataFrame(train_data)
        test_data = pd.DataFrame(test_data)
        train_matrix = np.zeros((n_users, n_items))
        test_matrix = np.zeros((n_users, n_items))
        for line in train_data.itertuples():
            train_matrix[line[1] - 1, line[2] - 1] = line[3]
        for line in test_data.itertuples():
            test_matrix[line[1] - 1, line[2] - 1] = line[3]

        return train_matrix, test_matrix

    def loadMovielens1M(self, path=PATH_1M):
        '''
        Load the Movielens 1M data set
        :param path: path of the dataset
        :return: train data and test data
        '''
        header = ['user_id', 'item_id', 'rating', 'timestamp']
        df = pd.read_csv(path, sep='::', names=header)
        n_users = df.user_id.unique().shape[0] #n_users = 6040
        n_items = df.item_id.unique().shape[0] #n_items = 3952
        print("Number of users=" + str(n_users) + "; Number of items=" + str(n_items))
        train_data, test_data = train_test_split(df, test_size=self.test_ratio)
        train_data = pd.DataFrame(train_data)
        test_data = pd.DataFrame(test_data)
        train_matrix = np.zeros((n_users, n_items))
        test_matrix = np.zeros((n_users, n_items))
        for line in train_data.itertuples():
            train_matrix[line[1] - 1, line[2] - 1] = line[3]
        for line in test_data.itertuples():
            test_matrix[line[1] - 1, line[2] - 1] = line[3]

        return train_matrix, test_matrix